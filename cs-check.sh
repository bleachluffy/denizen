#!/bin/bash

shopt -s -o nounset

declare -x EXIT_CODE=0
declare -x FILES_QUANTITY=0

# Statistics
declare -xi QUANTITY_OF_FILES=0
declare -xi QUANTITY_OF_FAILUES=0
declare -xi QUANTITY_OF_OK=0
declare -x  ERRORS_LIST=""

printf "\nCheck codestandards \n\n"

for file in scripts/*.yml; do
    declare -x ERROR_TEST_NAME=false
    declare -x ERROR_TABS=false
    declare -x ERROR_TRAILING_SPACE=false

    declare -x TEMP_ERRORS=""

    let QUANTITY_OF_FILES++

    # Check for trailing spaces
    declare -x HAS_TRAILING_SPACES=$(sed -n '/ \+$/p' "$file" | wc -l)
    if [[ "$HAS_TRAILING_SPACES" -gt "0" ]]; then
       ERROR_TRAILING_SPACE=true
        TEMP_ERRORS="$TEMP_ERRORS - Remove trailing spaces in the code - Supprimer les espaces en fin de ligne dans le code\n"
    fi

    # Check if files are using "tabs", we should use only spaces to ident files
    declare -x COUNT_OF_TABS_IN_LINES=$(cat "$file" | grep -P "\t" | wc -l)
    if [[ "$COUNT_OF_TABS_IN_LINES" -gt "0" ]]; then
        ERROR_TABS=true
        TEMP_ERRORS="$TEMP_ERRORS - Only spaces are allowed for indentation - Seuls les espaces sont autorisés pour l'indentation\n"
    fi

    # Check if files begin with a lowercase
    declare -x FIRST_LETTER_NAME_FILE=$(find "$file" -type f -name "[^[:lower:]]*" | wc -l)
    if [[ "$FIRST_LETTER_NAME_FILE" -gt "0" ]]; then
        ERROR_TEST_NAME=true
        TEMP_ERRORS="$TEMP_ERRORS - The file name must start with a lowercase - Le nom du fichier doit commencer par une minuscule\n"
    fi

    # Handle dotted output and store error information
    if [[ ${ERROR_TABS} = true || ${ERROR_TEST_NAME} = true || ${ERROR_TRAILING_SPACE} = true ]]; then
        echo -n 'F'
        let QUANTITY_OF_FAILUES++
        EXIT_CODE=1
        ERRORS_LIST="$ERRORS_LIST\n$file\n$TEMP_ERRORS"
    else
        echo -n '.'
        let QUANTITY_OF_OK++
    fi

done

if [[ "$QUANTITY_OF_FAILUES" -gt "0" ]];
then
    printf "\n\nThere was %d failure:\n" "$QUANTITY_OF_FAILUES"
    printf "$ERRORS_LIST"
    printf "\n\nFAILURE!\nFiles: %d, OK: %d, Failures: %d.\n" "$QUANTITY_OF_FILES" "$QUANTITY_OF_OK" "$QUANTITY_OF_FAILUES"
else
    printf "\n\nOK (files %d, ok %d)\n" "$QUANTITY_OF_FILES" "$QUANTITY_OF_OK"
fi

exit ${EXIT_CODE}
